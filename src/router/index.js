import Vue from "vue";
import VueRouter from "vue-router";
import Main from "../views/main.vue";

Vue.use(VueRouter);

const routes = [
  {
    path: "/",
    name: "main",
    component: Main
  },
  {
    path: "/book/:id",
    name: "book",
    component: () => import(/* webpackChunkName: "about" */ "../views/book.vue")
  }
];

const router = new VueRouter({
  mode: "history",
  base: process.env.BASE_URL,
  routes,
  scrollBehavior(to, from, savedPosition) {
    // TODO: передовать позицию скролла для списка в мета
    if (savedPosition) {
      return savedPosition;
    } else {
      return { x: 0, y: 0 };
    }
  }
});

export default router;
