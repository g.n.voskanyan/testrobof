import { format } from "date-fns";

export const bookMapper = res => {
  const book = res.volumeInfo;

  return {
    title: book.title,
    author: book.authors && book.authors.length ? book.authors[0] : "",
    date: book.publishedDate
      ? format(new Date(book.publishedDate), "yyyy")
      : "",
    description: book.description || "",
    imageUrl: (book.imageLinks && book.imageLinks.thumbnail) || "",
    rating: book.averageRating || 0
  };
};
