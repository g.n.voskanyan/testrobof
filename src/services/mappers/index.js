export { autocompleteMapper } from "./autocomplete.mapper";
export { bookMapper } from "./book.mapper";
export { bookListMapper } from "./book-list.mapper";
