const shortDescription = description => {
  if (description) {
    return description.substring(0, 145) + "...";
  }

  return "";
};

export const bookListMapper = res => ({
  totalCount: res.totalItems,
  items: res.items
    ? res.items.map(book => ({
        id: book.id,
        title: book.volumeInfo.title,
        description: shortDescription(book.volumeInfo.description),
        imageUrl: book.volumeInfo.imageLinks
          ? book.volumeInfo.imageLinks.smallThumbnail
          : "",
        rating: book.volumeInfo.averageRating || 0
      }))
    : []
});
