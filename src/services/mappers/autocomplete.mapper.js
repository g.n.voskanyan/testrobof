export const autocompleteMapper = res =>
  res.items ? res.items.map(book => book.volumeInfo.title) : [];
