import axios from "axios";

import { bookListMapper, autocompleteMapper, bookMapper } from "./mappers";

const DEFAULT_REQUEST_PARAMS = {
  q: "",
  startIndex: 0,
  maxResults: 4,
  orderBy: "relevance"
};

class ApiService {
  _host = "https://www.googleapis.com/books/v1/";
  _bookListUrl = `${this._host}volumes`;

  async fetchBookList(p) {
    const params = { ...DEFAULT_REQUEST_PARAMS, ...p };

    const res = await axios({
      method: "GET",
      url: this._bookListUrl,
      params
    });

    return res.data;
  }

  async fetchBook(id) {
    const url = `${this._bookListUrl}/${id}`;
    let res = null;

    try {
      res = await axios.get(url);
    } catch (err) {
      console.error(err);
      throw new Error("Не получилось найти эту книгу");
    }

    return bookMapper(res.data);
  }

  async getBookList(params) {
    let res = [];
    try {
      res = await this.fetchBookList(params);
    } catch (err) {
      console.error(err);
      throw new Error(
        "Ошибка, при запросе за книгами. Повторите попыткку позже."
      );
    }

    return bookListMapper(res);
  }

  async getAutocomplete(q) {
    const params = {
      q,
      startIndex: 0,
      maxResults: 15,
      orderBy: "relevance"
    };
    let res = [];

    try {
      res = await this.fetchBookList(params);
    } catch (err) {
      console.error(err);
      throw new Error("Автокомплит временно не работает.");
    }

    return autocompleteMapper(res);
  }
}

export const api = new ApiService();
