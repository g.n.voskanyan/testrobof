import injector from "vue-inject";
import { BehaviorSubject } from "rxjs";

export const DEFAULT_CONFIG = {
  medium: 769,
  large: 1200
};

export class ViewportSizeService {
  _brackepoint = DEFAULT_CONFIG;
  _currentSize = "large";
  _viewportSize$ = new BehaviorSubject(this._currentSize);

  _setViewportSize(viewportWidth) {
    if (
      viewportWidth < this._brackepoint.medium &&
      this._currentSize !== "small"
    ) {
      this._currentSize = "small";
      this._viewportSize$.next(this._currentSize);
    }

    if (
      viewportWidth >= this._brackepoint.large &&
      this._currentSize !== "large"
    ) {
      this._currentSize = "large";
      this._viewportSize$.next(this._currentSize);
    }

    if (
      this._currentSize !== "medium" &&
      viewportWidth < this._brackepoint.large &&
      viewportWidth >= this._brackepoint.medium
    ) {
      this._currentSize = "medium";
      this._viewportSize$.next(this._currentSize);
    }
  }

  get viewportSize() {
    return this._viewportSize$.asObservable();
  }

  constructor(config = DEFAULT_CONFIG) {
    this._brackepoint.medium = config.medium;
    this._brackepoint.large = config.large;
    this.observeViewportSize();
  }

  observeViewportSize() {
    const resizeObs = new ResizeObserver(entris => {
      this._setViewportSize(entris[0].contentRect.width);
    });
    resizeObs.observe(document.querySelector("body"));
  }
}
injector.service("viewportSizeService", ViewportSizeService);
