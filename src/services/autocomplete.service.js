import injector from "vue-inject";
import { BehaviorSubject } from "rxjs";
import { debounceTime } from "rxjs/operators";
import { api } from "./api.service";

class AutocompleteService {
  _query$ = new BehaviorSubject("");
  _autocomplete$ = new BehaviorSubject([]);

  constructor() {
    this.init();
  }

  async _getAutocomplete(q) {
    const list = q ? await api.getAutocomplete(q) : [];
    this._autocomplete$.next(list);
  }

  get list() {
    return this._autocomplete$.asObservable();
  }

  init() {
    this._query$.pipe(debounceTime(200)).subscribe(q => {
      this._getAutocomplete(q);
    });
  }

  changeQuery(q) {
    this._query$.next(q);
  }
}
injector.service("autocomplete", AutocompleteService);
