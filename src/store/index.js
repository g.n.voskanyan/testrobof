import Vue from "vue";
import Vuex from "vuex";
import { api } from "../services/api.service";

Vue.use(Vuex);

export default new Vuex.Store({
  state: {
    books: {
      totalCount: 0,
      items: []
    },
    book: null,
    currentPage: 1
  },
  getters: {
    totalCount: state => {
      return state.books.totalCount;
    },
    bookList: state => {
      return state.books.items;
    }
  },
  mutations: {
    setBooks(state, books) {
      state.books.totalCount = books.totalCount;
      state.books.items = books.items;
    },
    pushBooks(state, books) {
      state.books.totalCount = books.totalCount;
      state.books.items.push(...books.items);
    },
    setBook(state, book) {
      state.book = book;
    },
    clearBooks(state) {
      state.books.totalCount = 0;
      state.books.items = [];
    },
    setCurrentPage(state, page) {
      state.currentPage = page;
    }
  },
  actions: {
    async getBookList({ commit }, params = {}) {
      if (params.q) {
        const bookList = await api.getBookList(params);
        commit("setBooks", bookList);
      } else {
        commit("clearBooks");
      }
    },
    async uploadMoreBooks({ commit }, params = {}) {
      if (params.q) {
        const bookList = await api.getBookList(params);
        commit("pushBooks", bookList);
      } else {
        commit("clearBooks");
      }
    },
    async getBook({ commit }, id) {
      const book = await api.fetchBook(id);
      commit("setBook", book);
    },
    setCurrentPage({ commit }, page = 1) {
      commit("setCurrentPage", page);
    }
  }
});
