export const viewportSizeMixin = {
  dependencies: "viewportSizeService",
  data() {
    return {
      viewportSize: "",
      subs: null
    };
  },
  mounted() {
    this.subs = this.viewportSizeService.viewportSize.subscribe(size => {
      this.viewportSize = size;
    });
  },
  destroyed() {
    this.subs.unsubscribe();
  }
};
