import Vue from "vue";
import App from "./App.vue";
import router from "./router";
import store from "./store";
import injector from "vue-inject";
import VTooltip from "v-tooltip";
import InfiniteLoading from "vue-infinite-loading";

import "normalize.css";
import "./assets/styles.css";

Vue.config.productionTip = false;

require("./services/autocomplete.service");
require("./services/viewport-size.service");

Vue.use(injector);
Vue.use(VTooltip);
Vue.use(InfiniteLoading);

new Vue({
  router,
  store,
  render: h => h(App)
}).$mount("#app");
