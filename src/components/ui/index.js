export { default as SearchInput } from "./search-input.vue";
export { default as IconBtn } from "./icon-btn.vue";
export { default as Rate } from "./rate.vue";
export { default as Pagination } from "./pagination.vue";
export { default as Tooltip } from "./tooltip.vue";
export { default as LabelText } from "./label-text.vue";
